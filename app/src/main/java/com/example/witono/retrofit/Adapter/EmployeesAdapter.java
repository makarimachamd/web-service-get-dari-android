package com.example.witono.retrofit.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.witono.retrofit.Employee.Logistik;
import com.example.witono.retrofit.R;

import java.util.List;

public class EmployeesAdapter extends RecyclerView.Adapter<EmployeesAdapter.CustomViewHolder> {
    private List<Logistik> employees;

    public EmployeesAdapter(List<Logistik> employees) {
        this.employees = employees;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.employee_list, parent, false);

        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        Logistik employee = employees.get(position);
        holder.email.setText(employee.getNamaBarang());
        holder.designation.setText(employee.getJenis());
        holder.salary.setText(employee.getHarga());
        holder.dob.setText(employee.getTujuan());
        holder.contactNumber.setText(employee.getAsal());
        holder.employeeName.setText(employee.getStatus());
    }

    @Override
    public int getItemCount() {
        return employees.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public TextView employeeName, designation, email, salary, dob,contactNumber;

        public CustomViewHolder(View view) {
            super(view);
            employeeName = view.findViewById(R.id.employeeName);
            email = view.findViewById(R.id.email);
            designation = view.findViewById(R.id.designation);
            salary = view.findViewById(R.id.salary);
            dob = view.findViewById(R.id.dob);
            contactNumber = view.findViewById(R.id.contactNumber);
        }
    }
}