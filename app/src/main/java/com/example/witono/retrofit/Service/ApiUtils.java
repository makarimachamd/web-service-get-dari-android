package com.example.witono.retrofit.Service;

import com.example.witono.retrofit.Helper.RetrofitClient;
import com.example.witono.retrofit.Helper.RetrofitClientBantu;

public class ApiUtils {
    private ApiUtils() {}

    public static final String BASE_URL = "https://tcckelompokfmna.000webhostapp.com/";

    public static ApiService getAPIService() {

        return RetrofitClientBantu.getClient(BASE_URL).create(ApiService.class);
    }
}
