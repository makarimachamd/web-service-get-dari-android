package com.example.witono.retrofit.Employee;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogistikList {

    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("records")
    @Expose
    private List<Logistik> item = null;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<Logistik> getItem() {
        return item;
    }

    public void setItem(List<Logistik> item) {
        this.item = item;
    }

}