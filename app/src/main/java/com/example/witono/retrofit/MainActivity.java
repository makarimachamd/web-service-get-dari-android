package com.example.witono.retrofit;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.witono.retrofit.Adapter.EmployeesAdapter;
import com.example.witono.retrofit.Employee.Logistik;
import com.example.witono.retrofit.Employee.LogistikList;
import com.example.witono.retrofit.Helper.RetrofitClient;
import com.example.witono.retrofit.Service.ApiService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Logistik> employeeList;
    private ProgressDialog pDialog;
    private RecyclerView recyclerView;
    private EmployeesAdapter eAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading Data.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        //Creating an object of our api interface
        ApiService api = RetrofitClient.getApiService();

        /**
         * Calling JSON
         */
        Call<LogistikList> call = api.getMyJSON();

        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<LogistikList>() {
            @Override
            public void onResponse(Call<LogistikList> call, Response<LogistikList> response) {
                //Dismiss Dialog
                pDialog.dismiss();

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    employeeList = (ArrayList<Logistik>) response.body().getItem();
                    recyclerView = findViewById(R.id.recycler_view);
                    eAdapter = new EmployeesAdapter(employeeList);
                    RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(eLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(eAdapter);
                }
            }

            @Override
            public void onFailure(Call<LogistikList> call, Throwable t) {
                pDialog.dismiss();
            }
        });
    }
}