package com.example.witono.retrofit.Employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Preview {
    @SerializedName("nama_barang")
    @Expose
    private String namabarang;
    @SerializedName("jenis")
    @Expose
    private String jenis;
    @SerializedName("harga")
    @Expose
    private String harga;
    @SerializedName("tujuan")
    @Expose
    private String tujuan;
    @SerializedName("asal")
    @Expose
    private String asal;
    @SerializedName("status")
    @Expose
    private String status;

    public String getNamabarang() {
        return namabarang;
    }

    public void setNamabarang(String namabarang) {
        this.namabarang = namabarang;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Post{" +
                "nama_barang='" + namabarang + '\'' +
                ", jenis='" + jenis + '\'' +
                ", harga=" + harga + '\'' +
                ", tujuan=" + tujuan + '\'' +
                ", asal=" + asal + '\'' +
                ", status=" + status + '\'' +
                '}';
    }
}
