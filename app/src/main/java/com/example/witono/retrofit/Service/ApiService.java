package com.example.witono.retrofit.Service;

import com.example.witono.retrofit.Employee.LogistikList;
import com.example.witono.retrofit.Employee.Preview;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface ApiService {

    /*
    Retrofit get annotation with our URL
    And our method that will return us the List of EmployeeList
    */
    @GET("getProduct.php")
    Call<LogistikList> getMyJSON();

    @POST("/create.php")
    @FormUrlEncoded
    Call<Preview> savePost(@Field("id") String id,
                           @Field("nama_barang") String namabarang,
                           @Field("jenis") String jenis,
                           @Field("harga") String harga,
                           @Field("tujuan") String tujuan,
                           @Field("asal") String asal,
                           @Field("status") String status);

}